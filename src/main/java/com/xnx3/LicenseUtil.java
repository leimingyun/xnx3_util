package com.xnx3;

import com.xnx3.DSAUtil;
import com.xnx3.DateUtil;
import com.xnx3.Lang;

/**
 * License 授权码
 * 
 * 1. 实现22行的唯一标识设置
 * 2. 生成授权码，参见 43 行
 * 3. 在 Application.java 启动完毕后 执行 LicenseUtil.startCheckThread()  开启授权检测，如果授权码过期了会自动结束当前系统运行
 * 
 * @author 管雷鸣
 *
 */
public class LicenseUtil {
	static DSAUtil dsa;
	public static String license;	//授权码
	static {
		
		/*
		 参考
		 https://www.jb51.net/article/235799.htm
		 获取CPU编号、网络mac地址 等信息作为唯一标识赋予uuid
		 */
		String uuid = "12345";
		
		dsa = new DSAUtil(uuid);
	}
	/**
	 * 传入当前电脑的唯一标识，比如cpu的编号、网络mac地址等
	 * @param uuid 唯一标识
	 */
	public LicenseUtil(String uuid) {
		dsa = new DSAUtil(uuid);
	}
	
	public static void main(String[] args) {
		LicenseUtil license = new LicenseUtil("1234");
		
		//根据 过期时间的时间戳，生成授权码
		String licenseCode = license.encrypt(DateUtil.timeForUnix10());
		System.out.println(licenseCode);
		
		//根据授权码，取过期的时间戳
		System.out.println(license.decrypt(licenseCode+"123"));
	}
	
	/**
	 * 对文本进行加密
	 * @param expiretime 过期时间的10位时间戳，过了这个时间，就过期了，不能运行了
	 * @return 加密后的授权码文本
	 */
	public static String encrypt(int expiretime) {
		return dsa.encrypt(expiretime+"");
	}
	
	/**
	 * 根据授权码，获取过期时间
	 * @param licenseCode
	 * @return
	 */
	public static int decrypt(String licenseCode) {
		String t="";
		try {
			dsa.decrypt(licenseCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Lang.stringToInt(t, 0);
	}
	
	/**
	 * 开启授权码的检测线程，这个是系统启动后就开始执行。授权码从 application.properties 中取  license=xxxx
	 */
	public static void startCheckThread() {
		if(license == null || license.length() < 1) {
			System.out.println("请在 application.properties 中设置 license=xxxx");
		}
		new Thread(new Runnable() {
			public void run() {
				System.out.println("start license thread ...");
				while(true) {
					
					//每分钟检测一次是否过期
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					if(DateUtil.timeForUnix10() > decrypt(license)) {
						//过期了，阻止继续使用
						System.out.println("授权过期，终止");
						System.exit(0);
					}
				}
			}
		}).start();
	}
}
