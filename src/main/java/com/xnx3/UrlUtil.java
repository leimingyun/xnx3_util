package com.xnx3;

import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * url网址相关操作
 * @author 管雷鸣
 */
public class UrlUtil {
	

	/**
	 * 静态资源文件的后缀
	 */
    public static final Set<String> STATIC_EXTENSIONS = new HashSet<>(Arrays.asList(
        // 图片文件
        "jpg", "jpeg", "png", "gif", "bmp", "webp", "svg", "ico",
        
        // 样式文件
        "css", "less", "sass", "scss",
        
        // JavaScript文件
        "js", "jsx", "ts", "tsx",
        
        // 字体文件
        "eot", "woff", "woff2", "ttf", "otf",
        
        // 文档文件
        "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "txt",
        
        // 音频文件
        "mp3", "wav", "ogg", "flac",
        
        // 视频文件
        "mp4", "webm", "ogv", "avi", "mov",
        
        // 压缩文件
        "zip", "rar", "7z", "tar", "gz",
        
        // 其他常见静态文件 - "xml"删除了，因为会有 sitemap.xml
        "csv", "md",
        
        // 3D和动画文件
        "obj", "fbx", "gltf", "glb", "swf",
        
        // 配置文件
        "htaccess", "yaml", "yml",
        
        // 地图文件
        "geojson", "kml", "kmz"
    ));
    
    /**
     * 检查给定的文件扩展名是否为静态资源
     * @param extension 文件扩展名（不包含点号）
     * @return 如果是静态资源则返回true，否则返回false
     */
    public static boolean isStaticResource(String extension) {
    	for (String ext : STATIC_EXTENSIONS) {
            if (ext.equalsIgnoreCase(extension)) {
                return true;
            }
        }
        return false;
    }
    
    /**
	 * 如果是静态资源（不需要进行翻译的网页），则返回true
	 * @param url 要判断的url网址，可以传入如： http://www.leimingyun.com/a/b/c.html?a=123  也可以传入相对的如 a/b/c.html?a=123 也可以传入文件如  c.html 
	 * @return true: 是静态文件，   false不是静态文件是要翻译的网页
	 */
	public static boolean isStaticFile(String url) {
		String requestUri = new String(url);
		
		String suffix = Lang.findFileSuffix(requestUri);
		if(suffix == null || suffix.length() == 0) {
			return false;
		}
		
		if(isStaticResource(suffix)) {
			return true;
		}else {
			return false;
		}
	}
	
	
	/**
	 * 替换掉路径的层级，即替换掉前缀的../  、  ./  、 /
	 * @param path 绝对路径前缀，如 http://www.wang.market/a/
	 * @param originalUrl 具体文件，如 ../js/func.js
	 * @return 替换好的字符串
	 */
	public static String hierarchyReplace(String path, String originalUrl){
		if(isAbsoluteUrl(originalUrl)){
			return originalUrl;
		}
		
		//提取出path中的域名
		String domain = "";		//如  http://wang.market   
		String s = path.substring(9, path.length());
		int domianInt = s.indexOf("/");
		if(domianInt > 0){
			domain = path.substring(0, domianInt+9);
		}
		
		//判断前缀路径末尾是否有/，如没有，补上
		if(path.lastIndexOf("/") != path.length()){
			path = path + "/";
		}
		
		//如果是跟路径引用，直接返回组合网址
		if(originalUrl.indexOf("/") == 0){
			return domain+originalUrl;
		}
		
		while(originalUrl.indexOf("./") == 0 || originalUrl.indexOf("../") == 0){
			if(originalUrl.indexOf("./") == 0){
				//过滤前面的./
				originalUrl = originalUrl.substring(2, originalUrl.length());
			}else if (originalUrl.indexOf("../") == 0) {
				//过滤前面的./
				originalUrl = originalUrl.substring(3, originalUrl.length());
				//prefixUrl要向上一级  
				path = path.substring(0, path.substring(0, path.length()-1).lastIndexOf("/")+1);
			}
		}
		
		return path+originalUrl;
	}
	
	/**
	 * 判断url是否是绝对路径网址
	 * @param url 要判断的url
	 * @return true:是绝对路径的
	 */
	public static boolean isAbsoluteUrl(String url){
		if(url.indexOf("http://") > -1 || url.indexOf("https://") > -1 || url.indexOf("//") > -1){
			return true;
		}
		return false;
	}
	
	/**
	 * 获取当前url地址所在的远程文件路径
	 * @param url 目标url地址，绝对路径，如 http://www.wscso.com/test/a.jsp
	 * @return url所在地址的路径，返回如 http://www.wscso.com/test/
	 */
	public static String getPath(String url){
		//排除问号及其后的字符串
		int wenhao = url.lastIndexOf("?");
		if(wenhao > -1){
			//发现问号的存在，那么将问号后面的字符删除掉
			url = url.substring(0, wenhao);
		}
		
		//排除 # 及其后的字符串
		int jinghao = url.lastIndexOf("#");
		if(jinghao > -1){
			//发现问号的存在，那么将问号后面的字符删除掉
			url = url.substring(0, jinghao);
		}
		
		
		int last = url.lastIndexOf("/");
		if(last > 8){
			url = url.substring(0, last+1);
		}
		
		return url;
	}
	
	/**
	 * 根据url地址，获取其访问的文件名字
	 * @param url 目标url，如 http://wang.market/images/a.jpg?a=123
	 * @return 返回访问的文件名，如以上url返回： a.jpg
	 */
	public static String getFileName(String url){
		//排除问号及其后的字符串
		int wenhao = url.lastIndexOf("?");
		if(wenhao > -1){
			//发现问号的存在，那么将问号后面的字符删除掉
			url = url.substring(0, wenhao);
		}
		
		//排除 # 及其后的字符串
		int jinghao = url.lastIndexOf("#");
		if(jinghao > -1){
			//发现问号的存在，那么将问号后面的字符删除掉
			url = url.substring(0, jinghao);
		}
		
		int last = url.lastIndexOf("/");
		if(last > 8){
			url = url.substring(last+1, url.length());
//			if(url.indexOf("?") > 0){
//				url = url.substring(0, url.indexOf("?"));
//			}
//			if(url.indexOf("#") > 0){
//				url = url.substring(0, url.indexOf("#"));
//			}
			return url;
		}else{
			//不是正常的url地址
			return "";
		}
	}
	
	

	/**
	 * 根据url地址，获取其访问的域名。若么有发现，返回空字符
	 * <p>这里传入的url必须是绝对路径的。如果不是绝对路径，也就是不带协议的化，如果是正常域名比如 xxx.com 这里是会原样返回的,但是如果不是正常域名，则会返回空字符串</p>
	 * @param url 目标url，如 http://wang.market/images/a.jpg
	 * @return 返回wang.market
	 */
	public static String getDomain(String url){
		if(url == null || url.length() == 0) {
			return url;
		}
		int start = url.indexOf("//");
		if(start > 0){
			url = url.substring(start+2, url.length());
			
			int end = url.indexOf("/");
			if(end > 0){
				url = url.substring(0, end);
			}
			return url;
		}else {
			String DOMAIN_REGEX = "^(?!-)(?!.*--)(?!.*-\\.)[A-Za-z0-9-]{1,63}(?<!-)(?:\\.[A-Za-z0-9-]{2,})+$";

			Pattern pattern = Pattern.compile(DOMAIN_REGEX);
			Matcher matcher = pattern.matcher(url);
			if(matcher.matches()) {
				return url;
			}else {
				return "";
			}
		}
	}
	
	/**
	 * 获取文件前缀名字。
	 * @param fileName 传入的文件名字。如传入 test.jpg ，则会返回不带后缀的名字： test
	 * @return 若传入null，则返回空字符串""
	 */
	public static String getFileBeforeName(String fileName){
		if(fileName == null){
			return "";
		}
		
		String htmlFile = "";
		if(fileName.indexOf(".") > 0){
			String s[] = fileName.split("\\.");
			htmlFile = s[0];
		}else{
			htmlFile = fileName;
		}
		return htmlFile;
	}
	
	/**
	 * 获取当前url的协议，返回如 http 、 https 、 ftp 、 file 等
	 * @param url 绝对路径，必须是全的，包含协议的，如 http://www.xnx3.com
	 * @return 自动截取的协议，如 http 。若取不到，则返回null
	 */
	public static String getProtocols(String url){
		if(url == null){
			return null;
		}
		
		if(url.indexOf("//") > 1){
			return url.substring(0, url.indexOf("//") - 1);
		}
		return null;
	}
	

	/**
	 * 获取当前url地址的访问路径。比如网址为 http://zvo.cn.com/a/b.html?c=123 那么这里输出为 /a/b.html?c=123
	 * @param url 目标url地址，绝对路径，传入如 http://zvo.cn.com/a/b.html?c=123
	 * @return 返回如 /a/b.html?c=123  
	 * 			<p>注意，如果传入如 http://zvo.cn.com 、 http://zvo.cn.com/ 这样的，则返回 /</p>
	 */
	public static String getRequestPath(String url){
		String domain = getDomain(url);
//		System.out.println(domain);
		
		//截取url 0 ~domain结束的部分
		String protocolsDomain = url.substring(0, url.indexOf(domain)+domain.length());
//		System.out.println(protocolsDomain);
		
		if(url.length() <= protocolsDomain.length()+1) {
			return "/";
		}
		
		//有后面的路径
		
		return url.substring(protocolsDomain.length(), url.length());
	}
	
	/**
	 * 传入一个文件名、或者文件的路径，获取其文件的后缀名
	 * @param text 如: xnx3.jar  或者 /Users/apple/xnx3.jar 或者 https://www.xnx3.com/a/b.jsp 注意，这里取的是最后一个 . 后面的文本，如果传入get参数，那就不准了， 可以结合 getFileName(...) 一起用 
	 * @return 后缀，如txt、jar......如果没有获得到，则返回null
	 */
	public static String getFileSuffix(String fileName) {
		if(fileName == null) {
			return null;
		}
		String suffix = Lang.findFileSuffix(fileName);
		return suffix;
	}
	
	/**
	 * 传入一个url，返回 这个url的协议+域名。
	 * <p>比如传入 http://wang.market/images/a.jpg 则返回 http://wang.market</p>
	 * @param url 传入的url，格式如 http://wang.market/images/a.jpg
	 * @return 返回如 http://wang.market
	 */
	public static String getProtocolsAndDomain(String url) {
		String pro = UrlUtil.getProtocols(url);
		String domain = UrlUtil.getDomain(url);
		return pro+"://"+domain;
	}
	
	/**
	 * 获取url中的get参数
	 * @param urlString 网址，绝对路径的
	 * @return get参数的键值对。如果没有传递get参数，那么这个map中也就是没有任何键值对了
	 */
	public static Map<String, String> getQueryParams(String urlString) {
		Map<String, String> queryParams = new HashMap<String, String>();
		try {
			URL url = new URL(urlString);
			String query = url.getQuery();
			if (query!= null) {
				String[] pairs = query.split("&");
				for (String pair : pairs) {
					int idx = pair.indexOf('=');
					String key = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
					String value = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");
					queryParams.put(key, value);
				}
			}
		} catch (Exception e) {
//		    return null;
		}
		return queryParams;
    }
	
	public static void main(String[] args) {
		String s = "http://2408035008.p.make.dcloud.portal1.portal.thefastmake.com";
		System.out.println(getDomain(s));
	}
}
