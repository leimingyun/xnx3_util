package com.xnx3;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

/**
 * 操作跟系统交互、以及调用相关
 * @author 管雷鸣
 */
public class SystemUtil {
	private static String osname = null;	//当前操作系统名字
	
	/**
	 * 调用当前系统的默认浏览器打开网页
	 * @param url 要打开网页的url
	 */
	public static void openUrl(String url){
		java.net.URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		try {
			java.awt.Desktop.getDesktop().browse(uri);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取Java运行时环境规范版本,如： 1.6 、1.7
	 * @return 数值 获取失败或转换失败返回 0 ，若成功，返回如： 1.7
	 */
	public static float getJavaSpecificationVersion(){
		String v = System.getProperty("java.specification.version");
		float xnx3_result = 0f;
		if(v == null || v.equals("")){
			xnx3_result = 0;
		}else{
			xnx3_result = Lang.stringToFloat(v, 0);
		}
		return xnx3_result;
	}
	
	/**
	 * 获取当前项目路径，用户的当前工作目录，如当前项目名为xnx3，则会获得其绝对路径 "E:\MyEclipseWork\xnx3"
	 * @return 项目路径
	 */
	public static String getCurrentDir(){
		return System.getProperty("user.dir");
	}
	
	/**
	 * 获取当前Java运行所依赖的Jre的路径所在，绝对路径
	 * @return 如：D:\Program Files\MyEclipse2014\binary\com.sun.java.jdk7.win32.x86_64_1.7.0.u45\jre
	 */
	public static String getJrePath(){
		return System.getProperty("java.home");
	}

	/**
	 * 设置剪切板文本内容
	 * @param content 内容
	 */
	public static void setClipboardText(String content){ 
		String vc = content.trim();
		StringSelection ss = new StringSelection(vc);
		java.awt.datatransfer.Clipboard sysClb=null;
		sysClb = Toolkit.getDefaultToolkit().getSystemClipboard();
		sysClb.setContents(ss,null);
	}
	
	/**
	 * 设置剪切板图片内容
	 * @param filePath 图片文件所在路径，如：E:\\MyEclipseWork\\refreshTaobao\\logScreen\\a.png 格式限制gif|png|jpg
	 */
	public static void setClipboardImage(String filePath){
		Image img = Toolkit.getDefaultToolkit().getImage(filePath);
		setClipboardImage(img);  //给剪切板设置图片型内容
	}
	
	/**
	 * 设置剪贴板图片内容
	 * @param image 图片
	 */
	public static void setClipboardImage(Image image) { 
		ImageSelection imgSel = new ImageSelection(image); 
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null); 
	} 
	
	/**
	 * 剪切版相关操作使用
	 * @author 管雷鸣
	 */
	private static class ImageSelection implements Transferable { 
	 	private Image image; 
		public ImageSelection(Image image) {
			this.image = image;
		} 
		  
		public DataFlavor[] getTransferDataFlavors() { 
			return new DataFlavor[]{DataFlavor.imageFlavor}; 
		} 
		public boolean isDataFlavorSupported(DataFlavor flavor) { 
			return DataFlavor.imageFlavor.equals(flavor); 
		}
		  
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException { 
			if (!DataFlavor.imageFlavor.equals(flavor)) {throw new UnsupportedFlavorException(flavor);} 
			return image; 
		}
	}
	
	/**
	 * 打开本地的文件夹
	 * @param filePath 路径，如 /Users/apple/Desktop/119/
	 */
	public static void openLocalFolder(String filePath){
		try {
			java.awt.Desktop.getDesktop().open(new File(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取当前操作系统名字
	 * @return 返回如： mac os x
	 */
	public static String getOSName(){
		if(osname == null){
			osname = System.getProperty("os.name").toLowerCase();
		}
		return osname;
	}
	
	/**
	 * 获取当前系统是否是 windows os
	 * @return true：是
	 */
	public static boolean isWindowsOS(){
		return getOSName().indexOf("window") > -1;
	}
	
	/**
	 * 获取当前系统是否是 mac
	 * @return true：是
	 */
	public static boolean isMacOS(){
		return getOSName().indexOf("mac") > -1;
	}
	
	/**
     * 获取当前系统CPU序列，可区分linux系统和windows系统
     */
    public static String getCpuId() throws Exception {
        String cpuId;
        // 获取当前操作系统名称
        String os = System.getProperty("os.name");
        os = os.toUpperCase();
        //System.out.println(os);

        // linux系统用Runtime.getRuntime().exec()执行 dmidecode -t processor 查询cpu序列
        // windows系统用 wmic cpu get ProcessorId 查看cpu序列
        if ("LINUX".equals(os)) {
            cpuId = getLinuxCpuId("dmidecode -t processor | grep 'ID'", "ID", ":");
        } else {
            cpuId = getWindowsCpuId();
        }
        return cpuId.toUpperCase().replace(" ", "");
    }

    /**
     * 获取linux系统CPU序列
     */
    public static String getLinuxCpuId(String cmd, String record, String symbol) throws Exception {
        String execResult = executeLinuxCmd(cmd);
        String[] infos = execResult.split("\n");
        for (String info : infos) {
            info = info.trim();
            if (info.indexOf(record) != -1) {
                info.replace(" ", "");
                String[] sn = info.split(symbol);
                return sn[1];
            }
        }
        return null;
    }

    public static String executeLinuxCmd(String cmd) throws Exception {
        Runtime run = Runtime.getRuntime();
        Process process;
        process = run.exec(cmd);
        InputStream in = process.getInputStream();
        BufferedReader bs = new BufferedReader(new InputStreamReader(in));
        StringBuffer out = new StringBuffer();
        byte[] b = new byte[8192];
        for (int n; (n = in.read(b)) != -1; ) {
            out.append(new String(b, 0, n));
        }
        in.close();
        process.destroy();
        return out.toString();
    }

    /**
     * 获取windows系统CPU序列
     */
    public static String getWindowsCpuId() throws Exception {
        Process process = Runtime.getRuntime().exec(
                new String[]{"wmic", "cpu", "get", "ProcessorId"});
        process.getOutputStream().close();
        Scanner sc = new Scanner(process.getInputStream());
        sc.next();
        String serial = sc.next();
        return serial;
    }
	
	public static void main(String[] args) {
		try {
			System.out.println(getCpuId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
