package com.xnx3.demo;

import com.xnx3.DateUtil;
import com.xnx3.interfaces.DelayCycleExecute;
/**
 * 延迟重试执行的demo
 * @author 管雷鸣
 *
 */
public class DelayCycleExecuteUtil {
	public static int i = 0;
	public static void main(String[] args) {
		
		com.xnx3.DelayCycleExecuteUtil execute = new com.xnx3.DelayCycleExecuteUtil(new DelayCycleExecute() {
			public void success() {
				System.out.println("执行成功了，会触发这个方法");
			}
			public void failure() {
				System.out.println("执行失败了。 如果延迟好几次执行都是失败，最后都没有成功，会触发这个方法");
			}
			public boolean executeProcedures(int i) {
				//这里只是简单的进行了示例， 如果i<3，这里return false,则是代表执行失败了，延迟一段时间继续尝试执行这里面的代码
				//当 return true时，代表执行成功了，整个立即停止执行，不需要再延迟执行了，并且会自动触发 success() 的方法执行
				//至于return true还是false，你自己在这里判断，到底是成功了呢还是失败了
				i = i + 1;
				System.out.println("我正在执行这里，此时i:"+i +", 时间是:"+DateUtil.currentDate("HH:mm:ss"));
				
				if(i < 4) {
					return false;
				}else {
					return true;
				}
			}
		});
		//设置每次执行的延迟时间，单位：毫秒。 这里是第一次立即执行，如果失败了，那么延迟 1秒后再次执行，如果还失败则延迟3秒在执行，如果还失败则延迟8秒再执行，如果还失败，那就不再执行了，而是立即触发 faiure() 方法
		//这里不止可以设置4个，可以随意组合，2个、3个、6个等都行
		execute.setSleepArray(new int[]{0,1000,3000,8000});
		//开始执行 - 这个是同步的，会造成阻塞。 如果想异步的，多线程方式，可以使用  execute.start()  开启一个新线程来执行
		execute.run();
		
	}
	
}
