package com.xnx3.net;


import cn.zvo.http.Http;
import cn.zvo.http.Response;

/**
 * 响应对象。请使用 {@link Http} 与之配套的  {@link Response}
 * @author 管雷鸣
 * @deprecated
 */
public class HttpResponse extends Response{
 
}
