package com.xnx3.net;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import com.xnx3.BaseVO;

/**
 * 网络通信
 * @author 管雷鸣
 */
public class NetUtil {
	
	/**
	 * 判断某个host、端口是否能连通，也就是端口是否打开了。通常用于启动前的端口是否被占用的检测
	 * @param host 可传入如 localhost、123.123.123.123
	 * @param port 端口号，传入如  80、 8080
	 * @return
	 */
	public static BaseVO ping(String host, int port) {
		BaseVO vo = new BaseVO();

		try {
			Socket Skt = new Socket(host, port);
		} catch (UnknownHostException e) {
			return BaseVO.failure(e.getMessage());
		} catch (IOException e) {
			return BaseVO.failure(e.getMessage());
		}

		return BaseVO.success("success");
	}
}
